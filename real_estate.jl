using Markdown
using InteractiveUtils

# ╔═╡ 87101dd0-0852-4987-9127-87f8c11b1a6e
using CSV, DataFrames, RDatasets


# ╔═╡ dc7b44ec-f900-4adc-8dc8-34463bebe56d
using Plots

# ╔═╡ 33ba68ee-1d08-4f26-b521-7ce2fcc6c114
using Flux

# ╔═╡ 17874bb4-6d74-49b7-860d-01343bf18094
using Flux.Data

# ╔═╡ 9b0397bf-fbe2-4ce3-9591-f348d35a6200
using Flux: logitcrossentropy, normalise, onecold, onehotbatch

# ╔═╡ ca9f9f7a-4af2-4138-80fc-054d3f5bb06b
using Flux: @epochs

# ╔═╡ 3c2fbb85-bbc6-4b95-a5f8-20e89b4087e6
using Markdown

# ╔═╡ a702b8de-ee9d-48c2-a385-6fe388a16d6d
using InteractiveUtils

# ╔═╡ 6a6cdac6-b2bd-4c72-8294-ca6b00f1b8bc
using Lathe.preprocess: TrainTestSplit

# ╔═╡ ef483bb9-3bfe-49b5-8a9a-bec905e3aceb
using Lathe.preprocess: StandardScaler, OrdinalEncoder, OneHotEncoder

df = CSV.read("real_estate.csv", DataFrame)

begin
	rename!(df, Symbol.(replace.(string.(names(df)), Ref(r"\[m\]"=>"_"))))
	rename!(df, Symbol.(replace.(string.(names(df)), Ref(r"\[s\]"=>"_"))))
	rename!(df, Symbol.(replace.(string.(names(df)), Ref(r"\s"=>"_"))))
end

size(df)

X = df[:, :X3_distance_to_the_nearest_MRT_station]
    y = df[:, :Y_house_price_of_unit_area]

cor(Matrix(df))

X_mat = Matrix(X)
Y_mat = Vector(y)
sized_data = 0.8
complete_data = size(X_mat)[1]
training_pointer = trunc(Int, sized_data * complete_data)

X_mat_training = X_mat[1: training_pointer, :]
X_mat_testing = X_mat[training_pointer+1: end, :]

function fun_loss(outcome::Vector{Float64}, features::Matrix{Float64}, weights::Vector{Float64})::Float64
    model = size(features)[1]
    hypot = features * weights
    loss = hypothesis - outcome
    cost = (1/(2model)) * (loss' * loss)
    return cost
end

function parameters(init_features::Matrix{Float64})::Tuple{Matrix{Float64}, Matrix{Float64}}
    feature_mean = mean(init_features, dims=2)
    functional_deviation = std(init_features, dims=2)
    return (feature_mean, functional_deviation)
end

function scale_features(features::Matrix{Float64}, sc_params::Tuple{Matrix{Float64}, Matrix{Float64}})::Matrix{Float64}
	normalised_features = (features .- sc_params[1]) ./ sc_params[2]
end

scaled_parameters = parameters(X_mat_training)

scaled_parameters[1]

last(scaled_parameters)

scaled_training_features = parameters(X_mat_training, scaled_parameters)
scaled_testing_features = parameters(X_mat_testing, scaled_parameters)

function training(features::Matrix{Float64}, outcome::Vector{Float64}, pheta::Float64, repeated::Int64)::Tuple{Vector{Float64}, Vector{Float64}}
    total_count = length(outcome)
    new_features = hcat(ones(total_count, 1), features)
    features_count = size(new_features)[2]
    weights = zeros(features_count)
    loss_values = zeros(repeated)

    for i in range(1, stop = repeated)
        prediction = new_features * weights
        loss_values[i] = get_loss(new_features, outcome, weights)
        weights = weights - ((pheta/total_count) * new_features') * (prediction - outcome)
    end
    return (weights, loss_values)
end


weights_errors = training(scaled_training_features, Y_mat_train, 0.04, 10000)

plot(weights_errors[2],
label="Cost",
	ylabel="Cost",
	xlabel="Number of Iteration", 
	title="Cost Per Iteration"
)

function get_predictions(features::Matrix{Float64}, weights::Vector{Float64})::Vector{Float64}
    total_count = size(features)[1]
    new_features = hcat(ones(total_count, 1), features)
    prediction = new_features * weights
    return prediction
end

function rmset(actual_outcome::Vector{Float64}, predicted_outcome::Vector{Float64})::Float64
    errors = predicted_outcome - actual_outcome
    squared_errors = errors .^ 2
    mse = mean(squared_errors)
    rmse = sqrt(mse)
    return rmse
end

training_predictions = get_predictions(scaled_training_features, weights_errors[1])
testing_predictions = get_predictions(scaled_testing_features, weights_errors[1])

rmset(Y_mat_train, training_predictions)

rmset(Y_mat_test, testing_predictions)

model = Dense(1,1,σ)

model.W
model.b

typeof(model.W)

methods(Flux.mse)

xHousing = [ [row.X3_distance_to_the_nearest_MRT_station] for row in eachrow(df)]


